package com.demo.demopush;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.onesignal.OneSignal;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;



public class ProfileActivity extends AppCompatActivity {
    Button lo,make;
    private final String LOG="MainActivity";
    Handler one,two;
    int i;

    private static final String NODE_USERS = "users";
    private FirebaseAuth mAuth;
    URL Url = null;
    InputStream is = null;
//    Bitmap bmImg = null;
    StringBuffer sb;
    String rId;
    EditText pickup,drop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        lo=(Button)findViewById(R.id.btnLogOut);
        make=(Button)findViewById(R.id.btnMakeRide);
        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("message/one/hour");
        pickup=(EditText)findViewById(R.id.txtCurrentLOc);
        drop=(EditText)findViewById(R.id.txtEndLoc);
        final DatabaseReference dbs= FirebaseDatabase.getInstance().getReference("rideStates");
        one=new Handler();
        rId=mAuth.getCurrentUser().getUid();
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (task.isSuccessful()) {
                            String token = task.getResult().getToken();
//                            trial(token);
                            saveToken(token);


                        } else {


                        }
                    }
                });
        make.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String p = pickup.getText().toString().trim();
                final String d = drop.getText().toString().trim();
                final StringBuffer sb = new StringBuffer("{");
                sb.append("\"riderId").append("\":\"").append("001").append("\"");
                sb.append(", \"riderCurrentLocation").append("\":\"").append(p).append("\"");
                sb.append(", \"riderEndLocation").append("\":\"").append(d).append("\"");
                sb.append(", \"state").append("\":\"").append("pending").append("\"");
                sb.append('}');

 //-------------------------------------------------------------------------------------------------save event to kafka topic
                final String kafkaJason = "http://192.168.1.156:8080/kafka/publish/" + sb;
                AsyncTaskExample asyncTask = new AsyncTaskExample();
                asyncTask.execute(kafkaJason);

                String y=mAuth.getCurrentUser().getUid();
                trial(y);

 //-------------------------------------------------------------------------------------------------set firebase to pending state
                dbs.child("activeRides").child(mAuth.getCurrentUser().getUid()).child("status").setValue("pending");


//--------------------------------------------------------------------------------------------------start Thread
                t1.start();





//                Toast.makeText(ProfileActivity.this, kafkaJason, Toast.LENGTH_LONG).show();
//                Intent go =new Intent(ProfileActivity.this,waitingAndOnGoing.class);
//                startActivity(go);
            }
        });

//        ------------------------------------------------------------------------------------------logOut
        lo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Intent ii=new Intent(ProfileActivity.this,MainActivity.class);
                startActivity(ii);
            }
        });
    }



    @Override
    protected void onStart() {
        super.onStart();
        if (mAuth.getCurrentUser() == null) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

//    ----------------------------------------------------------------------------------------------saving token
    private void saveToken(String token) {
        String email = mAuth.getCurrentUser().getEmail();
        User user = new User(email, token);

        DatabaseReference dbUsers = FirebaseDatabase.getInstance().getReference(NODE_USERS);

        dbUsers.child("riders").child(mAuth.getCurrentUser().getUid()).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(ProfileActivity.this, "Token Saved", Toast.LENGTH_LONG).show();
                }
            }
        });
    }



    //----------------------------------------------------------------------------------------------thread --> reading database
    Thread t1=new Thread(){
        @Override
        public void run() {
            for (i=0;i<100;i=i+2){

                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                one.post(new Runnable() {
                    @Override
                    public void run() {
                           read();
//                        Toast.makeText(ProfileActivity.this,"number : "+i,Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    };

    //----------------------------------------------------------------------------------------------read database status
    public void read(){

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        DatabaseReference mostafa = ref.child("rideStates").child("activeRides").child(mAuth.getCurrentUser().getUid()).child("status");

        mostafa.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String state = dataSnapshot.getValue(String.class);
                Toast.makeText(ProfileActivity.this,   state, Toast.LENGTH_LONG).show();
                //do what you want with the email

                if(state.equals("accepted")){
//                    kafkaRecord("accepted");
                    startNextActivity("accepted");

                }
//                Toast.makeText(ProfileActivity.this,   state, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
//                Toast.makeText(ProfileActivity.this, "complete : ", Toast.LENGTH_LONG).show();
            }
        });



    }
//   -----------------------------------------------------------------------------------------------start next activity
    public void startNextActivity(String state){
        kafkaRecord(state);

        Intent shw=new Intent(ProfileActivity.this,waitingAndOnGoing.class);
        startActivity(shw);
        t1.stop();
    }

    public void trial(String id){
        DatabaseReference m=FirebaseDatabase.getInstance().getReference();
//        m.child("id").setValue(mAuth.getCurrentUser().getUid());
        m.child("id").setValue(id);
    }

    public  void kafkaRecord(String state){



        final StringBuffer sb = new StringBuffer("{");
        sb.append("\"riderId").append("\":\"").append(rId).append("\"");
        sb.append(", \"riderCurrentLocation").append("\":\"").append("location").append("\"");
        sb.append(", \"riderEndLocation").append("\":\"").append("location").append("\"");
        sb.append(", \"state").append("\":\"").append(state).append("\"");
        sb.append('}');

        //--------------------------------------------------------------------------------------------------save event to kafka topic
        final String kafkaJason = "http://192.168.1.156:8080/kafka/publish/" + sb;
        ProfileActivity.AsyncTaskExample asyncTask = new ProfileActivity.AsyncTaskExample();
        asyncTask.execute(kafkaJason);
        Toast.makeText(ProfileActivity.this,state,Toast.LENGTH_LONG).show();
//        startNextActivity();


    }
    //    ----------------------------------------------------------------------------------------------asyncTask for kafka producer request
    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskExample extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... strings) {
            try {

                Url = new URL(strings[0]);
                HttpURLConnection conn = (HttpURLConnection) Url
                        .openConnection();
                conn.setDoInput(true);
                conn.connect();
                is = conn.getInputStream();


            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;


        }
    }
}
