package com.demo.demopush;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class waitingAndOnGoing extends AppCompatActivity {
    FirebaseAuth mAuth;
    Handler one, two;
    int i;
    TextView states;
    Button pay;
    URL Url = null;
    InputStream is = null;
    String rid;
    DatabaseReference ref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_and_on_going);
        ref = FirebaseDatabase.getInstance().getReference();
        one = new Handler();
        two = new Handler();
        mAuth = FirebaseAuth.getInstance();
        final String rid = mAuth.getCurrentUser().getUid();
        final DatabaseReference dbs = FirebaseDatabase.getInstance().getReference("rideStates");
        pay = (Button) findViewById(R.id.btnPay);
        states = (TextView) findViewById(R.id.txtstatus);
        pay.setEnabled(false);
//        Button cnsl=(Button)findViewById(R.id.btnCansel);
        t1.start();
//        cnsl.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });


//        ------------------------------------------------------------------------------------------pay button action
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                kafkaRecord("Payed");
                 removeRide();
//                 startNew();

            }
        });
    }


    //----------------------------------------------------------------------------------------------read database status
    public void read1() {

        DatabaseReference ref2 = FirebaseDatabase.getInstance().getReference();
        DatabaseReference mostafa = ref2.child("rideStates").child("activeRides").child(mAuth.getCurrentUser().getUid()).child("status");
        mostafa.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String state = dataSnapshot.getValue(String.class);
                //do what you want with the email
                if (state.equals("started")) {
//
//                    started();

                    kafkaRecord("started");
                    states.setText("              Enjoy your Ride");
//                    t2.start();


                }
//                else {
//                    Toast.makeText(waitingAndOnGoing.this,   state, Toast.LENGTH_LONG).show();
//
//                }
//               t2.start();
                else if (state.equals("ended")) {
                    kafkaRecord("ended");
                    states.setText("   Trip Ended..Pay for Your Ride");
                    pay.setEnabled(true);
                }
//                Toast.makeText(ProfileActivity.this,   state, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
//                Toast.makeText(ProfileActivity.this, "complete : ", Toast.LENGTH_LONG).show();
            }
        });


    }
    //----------------------------------------------------------------------------------------------read database status
//    public void read2(){
//
//        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
//        DatabaseReference mostafa = ref.child("rideStates").child("activeRides").child(mAuth.getCurrentUser().getUid()).child("status");
//        mostafa.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                String state = dataSnapshot.getValue(String.class);
//                //do what you want with the email
//                t1.stop();
//               if (state.equals("ended")){
//                   ended();
////                    kafkaRecord("ended");
////                    states.setText("   Trip Ended..Pay for Your Ride");
////                    pay.setEnabled(true);
////                    h=100;
////                    t2.stop();
//                }
////                Toast.makeText(ProfileActivity.this,   state, Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
////                Toast.makeText(ProfileActivity.this, "complete : ", Toast.LENGTH_LONG).show();
//            }
//        });
//
//
//
//    }


    //----------------------------------------------------------------------------------------------thread --> reading database
    Thread t1 = new Thread() {
        @Override
        public void run() {
            for (i = 0; i < 100; i = i + 2) {

                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                one.post(new Runnable() {
                    @Override
                    public void run() {

                        read1();

//                        }

//                        Toast.makeText(ProfileActivity.this,"number : "+i,Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
    };


    public void cansel() {


    }

    public void kafkaRecord(String state) {

        String rId = mAuth.getCurrentUser().getUid();

        final StringBuffer sb = new StringBuffer("{");
        sb.append("\"riderId").append("\":\"").append(rId).append("\"");
        sb.append(", \"riderCurrentLocation").append("\":\"").append("location").append("\"");
        sb.append(", \"riderEndLocation").append("\":\"").append("location").append("\"");
        sb.append(", \"state").append("\":\"").append(state).append("\"");
        sb.append('}');

        //--------------------------------------------------------------------------------------------------save event to kafka topic
        final String kafkaJason = "http://192.168.1.156:8080/kafka/publish/" + sb;
        waitingAndOnGoing.AsyncTaskExample asyncTask = new waitingAndOnGoing.AsyncTaskExample();
        asyncTask.execute(kafkaJason);


    }

    //    ----------------------------------------------------------------------------------------------asyncTask for kafka producer request
    @SuppressLint("StaticFieldLeak")
    private class AsyncTaskExample extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... strings) {
            try {

                Url = new URL(strings[0]);
                HttpURLConnection conn = (HttpURLConnection) Url
                        .openConnection();
                conn.setDoInput(true);
                conn.connect();
                is = conn.getInputStream();


            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;


        }
    }

    public void startNew() {

        Intent nw = new Intent(waitingAndOnGoing.this, ProfileActivity.class);
        startActivity(nw);


    }

    public void removeRide(){

        ref.child("rideStates").child("activeRides").child(mAuth.getCurrentUser().getUid()).setValue(null);
  startNew();


//        mostafa.child(rid).setValue(null);
    }

}